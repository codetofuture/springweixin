package com.weixin.msg;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="xml")
public class VoiceMsg extends Msg {
	private List<String> mediaIds;
	public VoiceMsg() {
		// TODO Auto-generated constructor stub
		this.msgType=MsgEmum.voice.toString();
		mediaIds=new ArrayList<String>();
	}
	@XmlElementWrapper(name="Voice")
	@XmlElement(name="MediaId")
	public List<String> getMediaIds() {
		return mediaIds;
	}
	public void AddMediaId(String mediaId){
		mediaIds.add(mediaId);
	}
}
