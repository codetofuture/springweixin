package com.weixin.service;

import java.io.File;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.dto.Signature;
import com.weixin.exception.WeixinException;
import com.weixin.vo.Article;
import com.weixin.vo.QunArticle;

/**
 * 微信服务类接口 
 * WeiXinService.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月15日 下午4:08:56 
 * micrxdd
 * 
 */
public interface WeiXinService {
    /**
     * 获取配置
     * @param key
     * @return String值
     */
    public String get(String key);
    /**
     * 检验微信的签名是否正确
     * @param signature
     * @return 布尔值
     */
    public boolean ChackSignature(Signature signature);
    /**
     * 获取AccessToken
     * @return StringToken
     * @throws WeixinException
     */
    public String getAccessToken() throws WeixinException;
    /**
     * 创建菜单 传入json字符串
     * @param json
     * @throws WeixinException
     */
    public void createMenu(String json) throws WeixinException;
    /**
     * 获取所有的菜单
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject findMenu() throws WeixinException;
    /**
     * 删除菜单
     * @throws WeixinException
     */
    public void deleteMenu() throws WeixinException;
    /**
     * 创建用户分组
     * @param groupname 组的名称
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject createGroup(String groupname)throws WeixinException;
    /**
     * 获取所有的分组
     * 
     * @return JSONObject JSONObject
     * @throws WeixinException
     */
    public JSONObject findAllGroups() throws WeixinException;
    /**
     * 根据openid查找用户
     * 
     * @param accessToken
     * @param openid
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject findUserGroup(String openid) throws WeixinException;
    /**
     * 修改分组名
     * 
     * @param accessToken
     * @param id
     *            分组id，由微信分配
     * @param name
     *            分组名字（30个字符以内）
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject updateName(Integer id, String name)throws WeixinException;
    /**
     * 移动用户分组
     * 
     * @param accessToken
     * @param openid
     *            用户唯一标识符
     * @param to_groupid
     *            分组id
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject membersMove(String openid, String to_groupid)
	    throws WeixinException;
    /**
     * 发送文本客服消息
     * 
     * @param openId
     * @param text
     * @return JSONObject
     * @throws WeixinException
     */
    public String sendText(String openId, String text) throws WeixinException;
    /**
     * 发送语音回复
     * 
     * @param openId
     * @param media_id
     * @return JSONObject
     * @throws WeixinException
     */
    public String SendVoice(String openId, String media_id) throws WeixinException;
    /**
     * 发送图片消息
     * 
     * @param openId
     * @param media_id
     * @return JSONObject
     * @throws WeixinException
     */
    public String SendImage(String openId, String media_id) throws WeixinException;
    /**
     * 发送视频回复
     * 
     * @param openId
     * @param media_id
     * @param title
     * @param description
     * @return JSONObject
     * @throws WeixinException
     */
    public String SendVideo(String openId, String media_id, String title,
	    String description) throws WeixinException;
    /**
     * 发送音乐回复
     * 
     * @param openId
     * @param musicurl
     * @param hqmusicurl
     * @param thumb_media_id
     * @param title
     * @param description
     * @return JSONObject
     * @throws WeixinException
     */
    public String SendMusic(String openId, String musicurl, String hqmusicurl,
	    String thumb_media_id, String title, String description)
	    throws WeixinException;
    /**
     * 发送图文回复
     * 
     * @param openId
     * @param articles
     * @return JSONObject
     * @throws WeixinException
     */
    public String SendNews(String openId, List<Article> articles)
	    throws WeixinException;
    /**
     * 上传图文消息素材
     * 
     * @param articles
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject uploadnews(List<QunArticle> articles)
	    throws WeixinException;
    /**
     * 根据分组进行群发
     * 
     * @param groupId
     * @param mpNewsMediaId
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject massSendall(String groupId, String mpNewsMediaId)
	    throws WeixinException;
    /**
     * 根据OpenID列表群发
     * 
     * @param openids
     * @param mpNewsMediaId
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject massSend(String[] openids, String mpNewsMediaId)
	    throws WeixinException;

    /**
     * 删除群发
     * 
     * @param msgid
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject massSend(String msgid) throws WeixinException;
    /**
     * 拉取用户信息
     * 
     * @param openid
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject getUserInfo(String openid) throws WeixinException;
    /**
     * 获取帐号的关注者列表
     * 
     * @param next_openid
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject getFollwersList(String next_openid)
	    throws WeixinException;
    /**
     * 创建临时二维码
     * 
     * @param accessToken
     * @param expireSeconds
     *            最大不超过1800
     * @param sceneId
     *            场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
     * @return JSONObject JSONObject
     * @throws WeixinException
     */
    public JSONObject createScene(int expireSeconds, int sceneId)
	    throws WeixinException;
    /**
     * 创建永久二维码
     * 
     * @param sceneId
     * @return
     * @throws WeixinException
     */
    public JSONObject createLimitScene(int sceneId) throws WeixinException;
    /**
     * 获取查看二维码链接
     * 
     * @param ticket
     * @return StringUrl
     */
    public String showqrcodeUrl(String ticket);
    /**
     * 
     * @param file
     * @param typp
     * @return
     */
    public JSONObject fileUpload(File file,String type) throws WeixinException ;
    
}
