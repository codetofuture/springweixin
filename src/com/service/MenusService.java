package com.service;

import java.util.List;

import com.dto.MenuDto;
import com.entity.Menu;
import com.entity.User;
import com.vo.BaseMenuVo;
import com.vo.MenuVo;
/**
 * 用户菜单服务接口
 * MenusService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:50:27 
 * @version
 * @user micrxdd
 */
public interface MenusService {
    /**
     * 直接返回实体列表
     * @param user
     * @return
     */
    public List<Menu> menusBase(User user);
    /**
     * 返回菜单 根据用户
     * @return
     */
    public List<MenuVo> Menus(User user);
    /**
     * 返回所有的菜单
     * @return
     */
    public List<MenuVo> AllMenus();
    /**
     * 返回所有菜单的dto对象
     * @return
     */
    public List<MenuDto> AllMenusBase();
    /**
     * 根据用户组获取相应菜单
     * @param groupid
     * @return
     */
    public List<BaseMenuVo> ListMenusByGroupId(Integer groupid);
    /**
     * 保存菜单
     * @param vos
     */
    public void SaveMenus(List<MenuDto> vos);
    /**
     * 更新菜单
     * @param dtos
     */
    public void UpdateMenus(List<MenuDto> dtos);
    /**
     * 删除菜单
     * @param ids
     */
    public void DeleteMenus(Integer[] ids);
}
