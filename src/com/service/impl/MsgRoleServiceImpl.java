package com.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.dao.MsgprsDao;
import com.dao.MsgroleDao;
import com.dto.MsgRoleDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.Msgprs;
import com.entity.Msgrole;
import com.service.MsgRoleService;
import com.weixin.util.WeixinMsg;
/**
 * 消息规则服务类
 * MsgRoleServiceImpl.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:18:35 
 * @version
 * @user micrxdd
 */
@Service("msgRoleService")
public class MsgRoleServiceImpl implements MsgRoleService {
    @Resource
    private MsgroleDao msgroleDao;
    @Resource
    private MsgprsDao msgprsDao;
    @Override
    public Pagers MsgRoleList(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	Pagers pagers=msgroleDao.getForPage(pageinfo);
	List<MsgRoleDto> dtos=new ArrayList<MsgRoleDto>();
	List<Msgrole> msgroles=(List<Msgrole>) pagers.getRows();
	for (Msgrole msgrole : msgroles) {
	    MsgRoleDto dto=new MsgRoleDto();
	    BeanUtils.copyProperties(msgrole, dto,"msgprs");
	    Msgprs msgprs=msgrole.getMsgprs();
	    if(msgprs!=null){
		dto.setPrsid(msgprs.getId());
		    dto.setPrsname(msgprs.getName());
	    }
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }

    @Override
    public void SaveMsgRoles(List<MsgRoleDto> dtos) {
	// TODO Auto-generated method stub
	for (MsgRoleDto msgRoleDto : dtos) {
	    Assert.notNull(msgRoleDto.getEvent(),"事件类型不能为空");
	    if(msgRoleDto.getEvent().equals(WeixinMsg.CLICK)){
		Assert.hasText(msgRoleDto.getEventkey(),"当事件类型为Click的时候必须要有输入Key参数");
	    }
	    Msgrole msgrole=new Msgrole();
	    BeanUtils.copyProperties(msgRoleDto, msgrole,"id");
	    msgrole.setMsgprs(msgprsDao.findById(msgRoleDto.getPrsid()));
	    msgroleDao.save(msgrole);
	}
    }

    @Override
    public void UpdateMsgRoles(List<MsgRoleDto> dtos) {
	// TODO Auto-generated method stub
	for (MsgRoleDto msgRoleDto : dtos) {
	    if(msgRoleDto.getId()<0){
		Msgrole msgrole=new Msgrole();
		    BeanUtils.copyProperties(msgRoleDto, msgrole,"id");
		    msgrole.setMsgprs(msgprsDao.findById(msgRoleDto.getPrsid()));
		    msgroleDao.save(msgrole);
	    }else{
		Msgrole msgrole=msgroleDao.findById(msgRoleDto.getId());
		Msgprs msgprs=msgprsDao.findById(msgRoleDto.getPrsid());
		BeanUtils.copyProperties(msgRoleDto, msgrole,"id");
		msgrole.setMsgprs(msgprs);
	    }
	}
    }

    @Override
    public void DelMsgRoles(String ids) {
	// TODO Auto-generated method stub
	msgroleDao.DelByStringids(ids);
    }

    @Override
    public void DelMsgIntRoles(Integer[] ids) {
	// TODO Auto-generated method stub
	for (Integer id : ids) {
	    Msgrole msgrole=msgroleDao.findById(id);
	    //System.out.println(msgrole.getRtype());
	    //System.out.println(WeixinConstant.EVENT);
	    //System.out.println(msgrole.getRtype().equals(WeixinConstant.EVENT));
	    if(msgrole!=null&&msgrole.getRtype().equals(WeixinMsg.EVENTCase)){
		msgroleDao.del(msgrole);
	    }
	}
    }
    
}
