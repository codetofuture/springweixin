package com.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.dao.ArticleTypeDao;
import com.dto.ArticleTypeDto;
import com.dto.Pageinfo;
import com.entity.Articletype;
import com.service.ArticleTypeService;
import com.vo.ArticleTypeVo;
/**
 * 文章分类服务
 * ArticleTypeServiceImpl.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午5:39:33 
 * @version
 * @user micrxdd
 */
@Service("articleTypeService")
public class ArticleTypeServiceImpl implements ArticleTypeService{
    @Resource
    private ArticleTypeDao articleTypeDao;
    @Override
    public List<ArticleTypeDto> ArticleTypeList() {
	// TODO Auto-generated method stub
	List<Articletype> list=articleTypeDao.findAll();
	return f(list);
    }
    public List<ArticleTypeDto> f(List<Articletype> list){
	List<ArticleTypeDto> articleTypeDtos = null;
	if (list.size() > 0) {
	    Map<Integer, ArticleTypeDto> map=new HashMap<Integer, ArticleTypeDto>();
	    articleTypeDtos = new ArrayList<ArticleTypeDto>();
	    for (Articletype articletype : list) {
		ArticleTypeDto vo = new ArticleTypeDto();
		vo.setText(articletype.getName());
		vo.setId(articletype.getId());
		List<ArticleTypeDto> child = new ArrayList<ArticleTypeDto>();
		vo.setChildren(child);
		vo.setDes(articletype.getDescription());
		map.put(articletype.getId(), vo);
	    }
	    for (Articletype articletype : list) {
		Articletype pArticletype=articletype.getArticletype();
		if(pArticletype==null){
		    System.out.println("根节点:"+articletype.getName());
		    articleTypeDtos.add(map.get(articletype.getId()));
		}else{
		    System.out.println("设置叶子节点:"+articletype.getName()+">"+pArticletype.getName());
		    map.get(pArticletype.getId()).getChildren().add(map.get(articletype.getId()));
		}
	    }
	}
	return articleTypeDtos;
    }
    @Override
    public void SaveArticleType(ArticleTypeVo articleTypeVo) {
	// TODO Auto-generated method stub
	Integer id=articleTypeVo.getId();
	if(id!=null&&id>0){
	    UpdateArticleType(articleTypeVo);
	    return;
	}
	Articletype articletype=new Articletype();
	articletype.setName(articleTypeVo.getName());
	articletype.setDescription(articleTypeVo.getDes());
	if(articleTypeVo.getPid()!=null){
	    articletype.setArticletype(articleTypeDao.findById(articleTypeVo.getPid()));
	    
	}
	articleTypeDao.save(articletype);
    }
    @Override
    public void UpdateArticleType(ArticleTypeVo articleTypeVo) {
	// TODO Auto-generated method stub
	Articletype articletype=articleTypeDao.findById(articleTypeVo.getId());
	if(articletype!=null){
	    articletype.setName(articleTypeVo.getName());
	    articletype.setDescription(articleTypeVo.getDes());
	    Integer pid=articleTypeVo.getPid();
	    if(pid!=null){
		articletype.setArticletype(articleTypeDao.findById(pid));
	    }
	    
	}
    }
    @Override
    public void DelArticleType(Integer id){
	articleTypeDao.DelById(id);
    }
    @Override
    public Object ArticleTyppById(Integer id) {
	// TODO Auto-generated method stub
	ArticleTypeVo vo=null;
	Articletype articletype=articleTypeDao.findById(id);
	if(articletype!=null){
	    vo=new ArticleTypeVo();
	    vo.setId(id);
	    vo.setName(articletype.getName());
	    vo.setDes(articletype.getDescription());
	    Articletype articletype2=articletype.getArticletype();
	    if(articletype2!=null){
		vo.setPid((articletype2.getId()));
	    }
	}
	return vo;
    }
    @Override
    public List<Articletype> ArticleTypeRootList(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	return articleTypeDao.getForPageList(pageinfo, Restrictions.isNull("articletype"));
    }
    

}
