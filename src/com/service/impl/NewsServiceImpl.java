package com.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.NewsDao;
import com.dao.NewsItemDao;
import com.dto.NewsDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.News;
import com.entity.Newsitem;
import com.service.NewsService;
@Service("newsService")
public class NewsServiceImpl implements NewsService{
    @Resource
    private NewsDao newsDao;
    @Resource
    private NewsItemDao newsItemDao;

    @Override
    public Pagers NewsList(Pageinfo pageinfo, Integer itemid) {
	// TODO Auto-generated method stub
	Newsitem newsitem=new Newsitem();
	newsitem.setId(itemid);
	Pagers pagers=newsDao.getForPage(pageinfo, Restrictions.eq("newsitem", newsitem));
	List<News> newses=(List<News>) pagers.getRows();
	List<NewsDto> dtos=new ArrayList<NewsDto>();
	for (News news : newses) {
	    NewsDto dto=new NewsDto();
	    BeanUtils.copyProperties(news, dto);
	    dto.setItemid(itemid);
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }

    @Override
    public void SaveNews(NewsDto dto,Integer itemid) {
	// TODO Auto-generated method stub
	Newsitem newsitem=newsItemDao.findById(itemid);
	System.out.println(newsitem);
	if(newsitem!=null){
	    News news=new News();
	    BeanUtils.copyProperties(dto, news,"id","articleid");
	    news.setNewsitem(newsitem);
		newsDao.save(news);
	}
	
    }

    @Override
    public void UpdateNews(Integer[] ids,Integer itemsid) {
	// TODO Auto-generated method stub
	List<News> news=newsDao.findAll(Restrictions.in("id", ids));
	Newsitem newsitem=new Newsitem();
	newsitem.setId(itemsid);
	for (News news2 : news) {
	    news2.setNewsitem(newsitem);
	}
    }

    @Override
    public void DelNewsIds(Integer[] ids) {
	// TODO Auto-generated method stub
	List<News> news=newsDao.findAll(Restrictions.in("id", ids));
	for (News news2 : news) {
	    news2.setNewsitem(null);
	}
    }

    @Override
    public Pagers NewsListNoItem(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	Pagers pagers=newsDao.getForPage(pageinfo, Restrictions.isNull("newsitem"));
	List<News> newses=(List<News>) pagers.getRows();
	List<NewsDto> dtos=new ArrayList<NewsDto>();
	for (News news : newses) {
	    NewsDto dto=new NewsDto();
	    BeanUtils.copyProperties(news, dto);
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }

    @Override
    public void SaveNewsDtos(List<NewsDto> dtos) {
	// TODO Auto-generated method stub
	
	for (NewsDto newsDto : dtos) {
	    Newsitem newsitem=newsItemDao.findById(newsDto.getItemid());
		System.out.println(newsitem);
		if(newsitem!=null){
		    News news=new News();
		    BeanUtils.copyProperties(newsDto, news,"id","articleid");
		    news.setNewsitem(newsitem);
		    newsDao.save(news);
		}
	}
    }

    @Override
    public void UpdateNewsDtos(List<NewsDto> dto) {
	// TODO Auto-generated method stub
	for (NewsDto newsDto : dto) {
	    if(newsDto.getId()<0){
		Newsitem newsitem=newsItemDao.findById(newsDto.getItemid());
		System.out.println(newsitem);
		if(newsitem!=null){
		    News news=new News();
		    BeanUtils.copyProperties(dto, news,"id","articleid");
		    news.setNewsitem(newsitem);
		    newsDao.save(news);
		}
	    }else{
		Newsitem newsitem=newsItemDao.findById(newsDto.getItemid());
		System.out.println(newsitem);
		if(newsitem!=null){
		    News news=newsDao.findById(newsDto.getId());
		    if(news!=null){
			BeanUtils.copyProperties(newsDto, news,"id","articleid");
		    }
		}
	    }
	}
    }

    @Override
    public List<News> NewsCounts(Pageinfo pageinfo,Integer id) {
	// TODO Auto-generated method stub
	List<News> list=newsDao.getForPageList(pageinfo, Restrictions.eq("articletypeid", id));
	return list;
    }

    @Override
    public List<News> NewsByNew(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	return newsDao.getForPageList(pageinfo);
    }
    

}
