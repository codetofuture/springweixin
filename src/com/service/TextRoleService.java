package com.service;

import java.util.List;

import com.dto.Pageinfo;
import com.dto.Pagers;
import com.dto.TextRoleDto;
/**
 * 关键字规则接口
 * TextRoleService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:57:13 
 * @version
 * @user micrxdd
 */
public interface TextRoleService {
    /**
     * 返回关键字列表
     * @param pageinfo
     * @return
     */
    public Pagers TextRoleList(Pageinfo pageinfo);
    /**
     * 保存关键字规则
     * @param dtos
     */
    public void SaveTextRoles(List<TextRoleDto> dtos);
    /**
     * 更新关键字规则
     * @param dtos
     */
    public void UpdateTextRoles(List<TextRoleDto> dtos);
    /**
     * 删除关键字规则
     * @param ids
     */
    public void DelTextRoles(String ids);
    /**
     * 删除关键字规则
     * @param ids
     */
    public void DelTextRoles(Integer[] ids);
}
