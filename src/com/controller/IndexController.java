package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.dto.Pageinfo;
import com.entity.Articletype;
import com.entity.User;
import com.service.ArticleTypeService;
import com.service.NewsService;
import com.service.UserService;
import com.util.SHA1Util;
import com.vo.NewsCountVo;
import com.vo.UserVo;

/**
 * 
 * IndexController.java
 * 
 * @author microxdd
 * @time 创建时间：2014 2014年9月15日 上午12:02:41
 * @version
 * @user micrxdd
 */
@Controller
@SessionAttributes("managerUser")
public class IndexController {
    private Map<String, Object> map;
    @Resource
    private UserService userService;
    @Resource
    private NewsService newsService;
    @Resource
    private ArticleTypeService articleTypeService;
    private Pageinfo pageinfo;

    public IndexController() {
	// TODO Auto-generated constructor stub
	map = new HashMap<String, Object>();
	map.put("success", Boolean.TRUE);
	pageinfo = new Pageinfo();
	pageinfo.setOrder("desc");
	pageinfo.setRows(5);
	pageinfo.setSort("id");

    }

    @RequestMapping("index")
    public String index(Model model) {
	System.out.println("index init");
	System.out.println("--de f-");
	//List<Newstype> list = newsTypeService.NewsTypeRootList(pageinfo);
	List<Articletype> list = articleTypeService.ArticleTypeRootList(pageinfo);
	System.out.println(list.size());
	List<NewsCountVo> countVos = new ArrayList<NewsCountVo>();
	for (Articletype articletype : list) {
	    NewsCountVo vo=new NewsCountVo();
	    vo.setArticletype(articletype);
	    vo.setNews(newsService.NewsCounts(pageinfo,articletype.getId()));
	    countVos.add(vo);
	}
	model.addAttribute("countVos", countVos);
	model.addAttribute("news", newsService.NewsByNew(pageinfo));
	return "index.htm";
    }

    @RequestMapping("login")
    @ResponseBody
    public Object UserLogin(UserVo userVo, Model model) {
	Subject subject = SecurityUtils.getSubject();
	UsernamePasswordToken token = new UsernamePasswordToken(
		userVo.getUsername(), SHA1Util.encodeWhithKey(userVo
			.getPassword()), true);
	String msg = null;
	try {
	    System.out.println("----------------开始登陆-------------------");
	    subject.login(token);
	    System.out.println("-----------------结束登陆------------------");
	    System.out.println(" 重新查找用户存入session session");
	    User user = userService.findUser(userVo.getUsername());
	    user.getGroup().getMenusids();
	    model.addAttribute("managerUser", user);
	    System.out.println(user + "==================登陆," + user.getId()
		    + user.getUsername());

	    msg = "manager/";
	    map.put("msg", msg);
	    return map;
	} catch (UnknownAccountException uae) {
	    msg = "用户名不存在";
	} catch (IncorrectCredentialsException ice) {
	    msg = "密码错误";
	} catch (LockedAccountException lae) {
	    throw new RuntimeException("account lock");
	} catch (ExcessiveAttemptsException eae) {
	    throw new RuntimeException("pwd try to long");
	}
	// User user = userService.findUser("admin");
	// System.out.println(user.getGroup().getMenusids());
	// model.addAttribute("managerUser", user);
	Map<String, Object> maps = new HashMap<String, Object>();
	maps.put("msg", msg);
	maps.put("success", Boolean.FALSE);
	return maps;
    }

}
