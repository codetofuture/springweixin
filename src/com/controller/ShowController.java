package com.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.Article;
import com.service.ArticleService;

/**
 * ShowMsgController.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月13日 下午10:45:43 
 * micrxdd
 * 
 */
@Controller
public class ShowController {
    
    @Resource
    private ArticleService articleService;
    @RequestMapping("article/{id}.htm")
    public String ShowArticle(@PathVariable Integer id,Model model){
	Article article=articleService.FindById(id);
	System.out.println(article);
	Assert.notNull(article,"文章被删除或者已经被禁止显示");
	model.addAttribute("a",article);
	return "article.htm";
    }
}
