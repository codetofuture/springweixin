package com.entity;

/**
 * Html entity. @author MyEclipse Persistence Tools
 */

public class Html implements java.io.Serializable {

    // Fields

    private Integer id;
    private Article article;
    private String html;

    // Constructors

    /** default constructor */
    public Html() {
    }

    /** minimal constructor */
    public Html(Article article) {
	this.article = article;
    }

    /** full constructor */
    public Html(Article article, String html) {
	this.article = article;
	this.html = html;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Article getArticle() {
	return this.article;
    }

    public void setArticle(Article article) {
	this.article = article;
    }

    public String getHtml() {
	return this.html;
    }

    public void setHtml(String html) {
	this.html = html;
    }

}