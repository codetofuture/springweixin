package com.entity;

import java.sql.Timestamp;

/**
 * Article entity. @author MyEclipse Persistence Tools
 */

public class Article implements java.io.Serializable {

    // Fields

    private Integer id;
    private Articletype articletype;
    private User user;
    private News news;
    private String title;
    private Timestamp ctime;
    private Boolean isshow;
    private Html html;

    // Constructors

    /** default constructor */
    public Article() {
    }

    /** minimal constructor */
    public Article(News news) {
	this.news = news;
    }

    /** full constructor */
    public Article(Articletype articletype, User user, News news, String title,
	    Timestamp ctime, Boolean isshow, Html html) {
	this.articletype = articletype;
	this.user = user;
	this.news = news;
	this.title = title;
	this.ctime = ctime;
	this.isshow = isshow;
	this.html = html;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Articletype getArticletype() {
	return this.articletype;
    }

    public void setArticletype(Articletype articletype) {
	this.articletype = articletype;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public News getNews() {
	return this.news;
    }

    public void setNews(News news) {
	this.news = news;
    }

    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public Timestamp getCtime() {
	return this.ctime;
    }

    public void setCtime(Timestamp ctime) {
	this.ctime = ctime;
    }

    public Boolean getIsshow() {
	return this.isshow;
    }

    public void setIsshow(Boolean isshow) {
	this.isshow = isshow;
    }

    public Html getHtml() {
	return this.html;
    }

    public void setHtml(Html html) {
	this.html = html;
    }

}