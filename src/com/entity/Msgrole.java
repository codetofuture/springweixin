package com.entity;

/**
 * Msgrole entity. @author MyEclipse Persistence Tools
 */

public class Msgrole implements java.io.Serializable {

    // Fields

    private Integer id;
    private Msgprs msgprs;
    private String rtype;
    private String event;
    private String eventkey;
    private Boolean enable;

    // Constructors

    /** default constructor */
    public Msgrole() {
    }

    /** full constructor */
    public Msgrole(Msgprs msgprs, String rtype, String event, String eventkey,
	    Boolean enable) {
	this.msgprs = msgprs;
	this.rtype = rtype;
	this.event = event;
	this.eventkey = eventkey;
	this.enable = enable;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Msgprs getMsgprs() {
	return this.msgprs;
    }

    public void setMsgprs(Msgprs msgprs) {
	this.msgprs = msgprs;
    }

    public String getRtype() {
	return this.rtype;
    }

    public void setRtype(String rtype) {
	this.rtype = rtype;
    }

    public String getEvent() {
	return this.event;
    }

    public void setEvent(String event) {
	this.event = event;
    }

    public String getEventkey() {
	return this.eventkey;
    }

    public void setEventkey(String eventkey) {
	this.eventkey = eventkey;
    }

    public Boolean getEnable() {
	return this.enable;
    }

    public void setEnable(Boolean enable) {
	this.enable = enable;
    }

}