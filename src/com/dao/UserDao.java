package com.dao;

import com.entity.User;


public interface UserDao extends BaseDao<User>{
    public User finduserByName(String username);
}
