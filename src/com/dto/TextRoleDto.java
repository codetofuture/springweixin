package com.dto;

import com.entity.Msgprs;

public class TextRoleDto {
    private Integer id;
    private Msgprs msgprs;
    private String txtkey;
    private Boolean isstart;
    private Integer prsid;
    private String prsname;
    private Boolean enable;
    
    public Boolean getEnable() {
        return enable;
    }
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Msgprs getMsgprs() {
        return msgprs;
    }
    public void setMsgprs(Msgprs msgprs) {
        this.msgprs = msgprs;
    }
    public String getTxtkey() {
        return txtkey;
    }
    public void setTxtkey(String txtkey) {
        this.txtkey = txtkey;
    }
    public Boolean getIsstart() {
        return isstart;
    }
    public void setIsstart(Boolean isstart) {
        this.isstart = isstart;
    }
    public Integer getPrsid() {
        return prsid;
    }
    public void setPrsid(Integer prsid) {
        this.prsid = prsid;
    }
    public String getPrsname() {
        return prsname;
    }
    public void setPrsname(String prsname) {
        this.prsname = prsname;
    }
    
}
